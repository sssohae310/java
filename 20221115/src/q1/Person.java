package q1;

public class Person {

	public int ssn;
	public String name;
	public int address;
	
	public Person() {
		
	}
	
	public Person(int ssn, String name, int address) {
		this.ssn=ssn;
		this.name=name;
		this.address=address;
	}

	public int getSsn() {
		return ssn;
	}

	public String getName() {
		return name;
	}

	public int getAddress() {
		return address;
	}

	public void setSsn(int ssn) {
		this.ssn = ssn;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAddress(int address) {
		this.address = address;
	}
	
	
	
	
	
	
}
