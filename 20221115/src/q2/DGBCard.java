package q2;

public class DGBCard extends Card{
	
	public String company;
	public String cardStaff;

	

	public DGBCard(String cardNo, int validDate, int cvc, String cardStaff) {
		super(cardNo, validDate, cvc);
		company = "대구은행";
		this.cardStaff=cardStaff;
	}
	@Override
	public void showCardInfo() {
		System.out.println("카드 정보 (Card NO : " + cardNo + ", 유효기간 : " + validDate + ", CVC : " + cvc + ")");
		System.out.println("담당직원 - " + cardStaff + "," +  company);
	}
	
}
