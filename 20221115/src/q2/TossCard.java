package q2;

public class TossCard extends Card{

	public String company;
	public String cardStaff;
	



	public TossCard(String cardNo, String cardStaff) {
		company = "Toss";
		this.cardNo=cardNo;
		this.cardStaff=cardStaff;
	}


	@Override
	public void showCardInfo() {
		System.out.println("카드 정보 - Card NO, " + cardNo);
		System.out.println("담당직원 - " + cardStaff + "," +  company);
	}
	
	
	
	

}
