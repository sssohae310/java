package q3;

public class SimplePaymanet implements Payment{

	
	public double simplePaymentdRatio;
	
	public SimplePaymanet(double simplePaymentdRatio) {
	this.simplePaymentdRatio=simplePaymentdRatio;
	}
	
	
	@Override
	public int online(int price) {
		double onsim = simplePaymentdRatio + ONLINE_PAYMENT_RATIO;
		int online = price * (int)onsim;
		return online;
	}

	@Override
	public int offline(int price) {
		double offsim =  simplePaymentdRatio + OFFLINE_PAYMENT_RATIO;
		int offline = price * (int)offsim;
		return offline;
	}

	@Override
	public void showInfo() {
		System.out.println("*** 간편결제 시 할인정보");
		System.out.printf("온라인 결제시 총 할인율 : %.2f \n",simplePaymentdRatio + ONLINE_PAYMENT_RATIO);
		System.out.printf("오프라인 결제 시 총 할인율 : %.2f \n",simplePaymentdRatio + OFFLINE_PAYMENT_RATIO);
	}
	
	
	
}
