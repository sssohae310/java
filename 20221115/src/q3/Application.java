package q3;

public class Application {
public static void main(String[] args) {
	
	int price = 10000;
	
	Payment card = new CardPayment(0.08);
	card.showInfo();
	System.out.println("온라인 결제 금액 : " + card.online(price));
	System.out.println("오프라인 결제 금액 : " + card.offline(price));
	
	Payment simple = new SimplePaymanet(0.05);
	simple.showInfo();
	System.out.println("온라인 결제 금액 : " + simple.online(price));
	System.out.println("오프라인 결제 금액 : " + simple.offline(price));
	
}
}

//결제 금액 안 나오는데 왜 그런지 모르겠습니다,,, price값이 들어가도 연산이 안되는데 왜지...