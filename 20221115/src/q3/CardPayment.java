package q3;

public class CardPayment implements Payment{

	public double cardRatio;
	
	public CardPayment(double cardRatio) {
	this.cardRatio=cardRatio;
	}

	@Override
	public int online(int price) {
		double onca = cardRatio + Payment.ONLINE_PAYMENT_RATIO;
		int online = price * (int)onca;
		return online;
	}
	
	//이거 return (int)(price -price *(payment.ONLINE_PAYMENT_RATIO + cardRatio));

	@Override
	public int offline(int price) {
		double offca =  cardRatio + Payment.OFFLINE_PAYMENT_RATIO;
		int offline = price * (int)offca;
		return offline;
	}

	@Override
	public void showInfo() {
		System.out.println("*** 간편결제 시 할인정보");
		System.out.printf("온라인 결제시 총 할인율 : %.2f \n",cardRatio + ONLINE_PAYMENT_RATIO);
		System.out.printf("오프라인 결제 시 총 할인율 : %.2f \n",cardRatio + OFFLINE_PAYMENT_RATIO);
	}
	
	//이거도 앞에 Payment.붙여야했음 
	
	
	
	
	
	
}
